import urllib
import RPi.GPIO as GPIO
import MySQLdb as mdb
import numpy as np
import time

from xml.dom import minidom

def checkDeparture(route, stopNum):
    
    NextBUSUrl = "http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=sf-muni&r=" + route + "&s=" + stopNum + "&useShortTitles=true"
    
    nbXML = urllib.urlopen(NextBUSUrl)
	
    readXML = minidom.parse(nbXML)
     
    dtype = [('busNum', int), ('eta', float), ('tTag',int)]
    busTimesSeconds = np.array([(00,00,00)],dtype=dtype)

    stop = []

    for node in readXML.getElementsByTagName("predictions"):
	stop.extend([
			str(node.getAttribute("routeTitle")),
			str(node.getAttribute("stopTitle")),
			str(node.getAttribute("stopTag"))
		]) 
        for node in readXML.getElementsByTagName("prediction"):
            vehicle = node.getAttribute("vehicle")
            seconds = int(node.getAttribute('seconds'))
            tag = int(node.getAttribute('tripTag'))
            dirTag = str(node.getAttribute('dirTag'))
            
            tempArray = np.array((vehicle,seconds,tag), dtype = dtype)
            
            busTimesSeconds = np.append(busTimesSeconds, tempArray)


    busTimesSeconds = np.sort(busTimesSeconds,order='eta') #unnecessary
    
    for i in range(1,len(busTimesSeconds)):
            if busTimesSeconds['eta'][i] <= 120:
                    
                    con = mdb.connect('localhost', 'muniPi', 'muniPi123', 'muniPIData');
                   
                    with con: 
                        cur = con.cursor()
                        cur.execute("""Select * from route_data where route_data.tripTag = """ + str(busTimesSeconds['tTag'][i]) )
                        
                        temp = cur.fetchall()
                         
                        if len(temp) == 0:    
                            cur.execute("""INSERT INTO route_data(tripTag,
                                        trainNumber,
                                        Route_Name, 
                                        dirTag,
                                        startTime,
                                        startStopTag, 
                                        startStopTitle) 
                                VALUES(""" + str(busTimesSeconds['tTag'][i]) + """,
                                       """ + str(busTimesSeconds['busNum'][i])  + """,
                                        '""" + stop[0] + """',
                                        '""" + dirTag + """' ,
                                        NOW(),
                                        '""" + stop[2] + """',
                                        '""" + stop[1] + """')""")
                        else:
                            cur.execute("""UPDATE route_data 
                                            SET startTime = NOW()
                                            WHERE route_data.tripTag = """ + str(busTimesSeconds['tTag'][i]))
    return 

def checkArrival(route, stopNum):
    
    NextBUSUrl = "http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=sf-muni&r=" + route + "&s=" + stopNum + "&useShortTitles=true"
   
    nbXML = urllib.urlopen(NextBUSUrl)
	
    readXML = minidom.parse(nbXML)
    #busTimesMinutes = []
    
    dtype = [('busNum', int), ('eta', float), ('tTag',int)]
    busTimesSeconds = np.array([(00,00,00)],dtype=dtype)

    stop = []

    for node in readXML.getElementsByTagName("predictions"):
	stop.extend([
			str(node.getAttribute("routeTitle")),
			str(node.getAttribute("stopTitle")),
			str(node.getAttribute("stopTag"))
		]) 
        for node in readXML.getElementsByTagName("prediction"):
            vehicle = node.getAttribute("vehicle")
            seconds = int(node.getAttribute('seconds'))
            tag = int(node.getAttribute('tripTag'))
      
            tempArray = np.array((vehicle,seconds,tag), dtype = dtype)
      
            busTimesSeconds = np.append(busTimesSeconds, tempArray)


    busTimesSeconds = np.sort(busTimesSeconds,order='eta') #unnecessary
    
    for i in range(1,len(busTimesSeconds)):
            if busTimesSeconds['eta'][i] <= 120: 

                    con = mdb.connect('localhost', 'muniPi', 'muniPi123', 'muniPIData');
                   
                    with con: 
                        cur = con.cursor()
                        cur.execute("""Select * from route_data where route_data.tripTag = """ + str(busTimesSeconds['tTag'][i]) )
                        
                        temp = cur.fetchall()
                         
                        if len(temp) > 0:
                            #Time difference calculation not working
                            cur.execute("""UPDATE route_data 
                                            SET route_data.destinationTime = NOW(), route_data.destinationStopTag =""" + stop[2] + """,
                                            route_data.destinationStopTitle = '""" + stop[1] + """',
                                            route_data.transitTime = TIME_TO_SEC(TIMEDIFF(destinationTime, startTime))
                                            WHERE route_data.tripTag = """ + str(busTimesSeconds['tTag'][i]))

    return 


checkDeparture('N','3212')
time.sleep(15)
checkArrival('N', '4509')
time.sleep(15)
checkDeparture('N','4510')
time.sleep(15)
checkArrival('N', '5123')
time.sleep(15)

checkDeparture('N','3212')
time.sleep(15)
checkArrival('N', '4509')
time.sleep(15)
checkDeparture('N','4510')
time.sleep(15)
checkArrival('N', '5123')
time.sleep(15)

checkDeparture('N','3212')
time.sleep(15)
checkArrival('N', '4509')
time.sleep(15)
checkDeparture('N','4510')
time.sleep(15)
checkArrival('N', '5123')
time.sleep(15)

checkDeparture('N','3212')
time.sleep(15)
checkArrival('N', '4509')
time.sleep(15)
checkDeparture('N','4510')
time.sleep(15)
checkArrival('N', '5123')
time.sleep(15)

checkDeparture('N','3212')
time.sleep(15)
checkArrival('N', '4509')
time.sleep(15)
checkDeparture('N','4510')
time.sleep(15)
checkArrival('N', '5123')

