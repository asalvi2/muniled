#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import time

con = mdb.connect('localhost', 'muniPi', 'muniPi123', 'muniPIData');
 
with con:
    
    cur = con.cursor()
    #cur.execute("DROP TABLE IF EXISTS Writers")
   # cur.execute("CREATE TABLE N_Judah_Inbound(Id INT PRIMARY KEY AUTO_INCREMENT, \
   #              C VARCHAR(25))")
    cur.execute("""INSERT INTO route_data(trainNumber,
                                        Route_Name, 
                                        startTime,
                                        startStopTag, 
                                        startStopTitle,
                                        destinationStopTag,
                                        destinationStopTitle) 
                                VALUES(123456, 
                                        'N-Judah',
                                        NOW(),
                                        123,
                                        '9th and irving',
                                        500, 
                                        'folsom and embarcadero')""")
                                        
  
    